﻿using System;
using System.Reflection;
using System.Runtime.Intrinsics.X86;

namespace ArrayObject
{
    public static class ArrayTasks
    {
        /// <summary>
        /// Task 1
        /// </summary>
        public static void ChangeElementsInArray(int[] nums)
        {
            int left = 0;
            int right = nums.Length - 1;

            while (left < right)
            {
                if (nums[left] % 2 == 0 && nums[right] % 2 == 0)
                {
                    int temp = nums[left];
                    nums[left] = nums[right];
                    nums[right] = temp;
                }

                left++;
                right--;
            }
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int DistanceBetweenFirstAndLastOccurrenceOfMaxValue(int[] nums)
        {
            // TODO: delete code line below, write down your solution 
            // Hint: Don`t modify array 'nums'
            int maxValue = int.MinValue;
            int maxIndex = -1;
            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] > maxValue)
                {
                    maxValue = nums[i];
                    maxIndex = i;
                }
            }

            int firstMaxIndex = -1;
            int lastMaxIndex = -1;

            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] == maxValue)
                {
                    if (firstMaxIndex == -1)
                        firstMaxIndex = i;
                    lastMaxIndex = i;
                }
            }

            return Math.Abs(lastMaxIndex - firstMaxIndex);

        }


        /// <summary>
        /// Task 3 
        /// </summary>
        public static void ChangeMatrixDiagonally(int[,] matrix)
        {
            // TODO: delete code line below, write down your solution 
            // Hint: Don`t create or initialize array, work only with given 'matrix'
            int n = matrix.GetLength(0);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (j < i)
                    {
                        matrix[i, j] = 0;
                    }
                    else if (j > i)
                    {
                        matrix[i, j] = 1;
                    }
                }
            }

        }

    }
}
